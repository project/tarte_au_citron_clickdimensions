window.tarteaucitron = window.tarteaucitron || {services: []};

// ClickDimensions
window.tarteaucitron.services.clickdimensions = {
  "key": "clickdimensions",
  "type": "analytic",
  "name": "ClickDimensions",
  "uri": "https://clickdimensions.com/about/privacy-policy/",
  "needConsent": true,
  "cookies": ["cusid", "cuvid", "cuvon"],
  "js": function () {
    "use strict";
    const onClickDimensionsScriptLoad = () => {
      const config = window.tarteaucitron.user;
      const cdAnalytics = new clickdimensions.Analytics('analytics-eu.clickdimensions.com');
      window.cdAnalytics = cdAnalytics;
      cdAnalytics.setAccountKey(config.clickdimensionsAccountKey);
      if (config.clickdimensionsDomain) {
        cdAnalytics.setDomain(config.clickdimensionsDomain);
      }
      cdAnalytics.setScore(typeof (cdScore) == "undefined" ? 0 : (cdScore == 0 ? null : cdScore));
      cdAnalytics.trackPage();
    }
    window.tarteaucitron.addScript('//analytics-eu.clickdimensions.com/ts.js', 'clickdimensions', onClickDimensionsScriptLoad);
  }
};
