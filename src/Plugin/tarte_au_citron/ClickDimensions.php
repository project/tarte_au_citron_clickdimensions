<?php

namespace Drupal\tarte_au_citron_clickdimensions\Plugin\tarte_au_citron;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tarte_au_citron\ServicePluginBase;

/**
 * A ClickDimensions service plugin for "Tarte au citron".
 *
 * @TarteAuCitronService(
 *   id = "clickdimensions",
 *   title = @Translation("ClickDimensions")
 * )
 */
class ClickDimensions extends ServicePluginBase {

  /**
   * Account key field.
   *
   * @const string
   */
  public const FIELD_ACCOUNT_KEY = 'clickdimensionsAccountKey';

  /**
   * Domain field.
   *
   * @const string
   */
  public const FIELD_DOMAIN = 'clickdimensionsDomain';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        self::FIELD_ACCOUNT_KEY => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings_form = parent::settingsForm($form, $form_state);

    $settings_form[self::FIELD_ACCOUNT_KEY] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account key'),
      '#default_value' => $this->getSetting(self::FIELD_ACCOUNT_KEY),
      '#required' => TRUE,
    ];

    $settings_form[self::FIELD_DOMAIN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $this->getSetting(self::FIELD_DOMAIN),
    ];

    return $settings_form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLibraryName() {
    return 'tarte_au_citron_clickdimensions/tarte_au_citron_clickdimensions';
  }

}
